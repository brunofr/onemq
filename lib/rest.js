var connect = require('connect'),
    messageWriter = require('./messageWriter'),
    client = require('./store').createClient();

module.exports.createServer = function(scheme, hostname, port) {
  var routes = function(app) {

    app.get('/:wid/:cid/token', function(req, res) {
      // return one-time token for post, expire after 30 seconds
      var uuid = client.createToken(30);
      res.writeJSON(null, 200, {
        "Cache-Control" : "no-cache",
        "Content-Length" : "0",
        "Link" : "<" + req.urlbase + req.params.wid + '/' + req.params.cid +
          ";t=" + uuid + ">; rel=\"" + req.urlbase +"/rel/push-message\">"
      });
    });

    app.get('/:wid/:cid/first', function(req, res) {
      // peek (soft-get) the first (FIRST IN) message of this channel
      // reserves the message for deletion during a visibility window
      var wid = req.params.wid,
          cid = req.params.cid;
      client.peek(wid, cid, function(message) {
        res.writeJSON(message);
      });
    });

    app.del('/:wid/:cid/:receiptHandle', function(req, res) {
      // delete a message
      client.del(req.params.receiptHandle, function(message) {
        res.writeJSON(message);
      });
    });

    app.get('/:wid/:cid', function(req, res) {
      // get information about this channel, including settings, and link
      // headers to URIs that can be used to access and modify this resource
      var wid = req.params.wid,
          cid = req.params.cid;
      client.length(wid, cid, function(length) {
        var message = {"messages-outstanding" : length};
        res.writeJSON(message);
      });
    });

    app.post('/:wid/:cid', function(req, res) {
      // posts message onto channel, takes optional one-time token ($;t=$)
      // for conditional gets
      var wid = req.params.wid,
          cid = req.params.cid,
          tokens = /(.*);t=(.*)/.exec(cid);

      if (tokens != null) {
        cid = tokens[1];
        client.expireToken(tokens[2], function(flag) {
          if (flag === false) {
            res.writeError(403);
          } else {
            client.push(wid, cid, req.body, function(message) {
              res.writeJSON(message);
            });
          }
        });
      } else {
        client.push(wid, cid, req.body, function(message) {
          res.writeJSON(message);
        });
      }
    });

    app.get('/:wid', function(req, res) {
      // returns information about this workspace, including its active
      // channels and link headers to URIs that can be used to interact with
      // this resource
      var wid = req.params.wid;
      client.channels(wid, function(channels) {
        if (channels === null) {
          res.writeError(500);
        } else {
          for (var i = 0, len = channels.length; i < len; i++) {
            var cid = /(.*):(.*):(.*)/.exec(channels[i])[2];
            channels[i] = req.urlbase + wid + '/' + cid;
          }
          var message = {"channels" : channels};
          res.writeJSON(message);
        }
      });
    });

  };

  var server = connect.createServer(
             messageWriter(scheme, hostname, port),
             connect.bodyParser(),
             connect.router(routes)
  );
  server.hostname = hostname;
  return server;
};
