var guid = require('node-uuid'),
    hash = require('hashlib'),
    redis = require('redis');

//
// Client uses four Redis data-structures to operate:
//
//   1. Messages are stored keyed on their UUID as key/value pairs.
//   2. Channels are stored as a set, keyed on the workspace id.
//   3. Message identifiers are stored in a FIFO queue, keyed on the channel id.
//
var Client = function() {
  this.client = redis.createClient();
};

Client.prototype._workspaceKey = function(wid) {
  return wid + ":workspace";
};

Client.prototype._channelKey = function(wid, cid) {
  return wid + ":" + cid + ":channel";
};

Client.prototype._tokenKey = function(uuid) {
  return uuid + ":token";
};

Client.prototype._receiptHandleKey = function(receiptHandle) {
  return receiptHandle + ':handle';
};

Client.prototype.createToken = function(ttl) {
  // ttl is expressed in seconds
  var uuid = guid();
  this.client.setex(this._tokenKey(uuid), ttl, null);
  return uuid;
};

Client.prototype.expireToken = function(token, callback) {
  this.client.del(this._tokenKey(token), function(err, reply) {
    if (err) {
      console.log(err);
        callback(false);
    } else {
      if (reply === 1) {
        callback(true);
      } else {
        callback(false);
      }
    }
  });
};

Client.prototype.push = function(wid, cid, msg, callback) {
  // all channels are stored in sets, one set per wid
  // is this channel already registered?
  var channelKey = this._channelKey(wid, cid);
  var workspaceKey = this._workspaceKey(wid);
  var that = this;
  this.client.sismember(workspaceKey, channelKey, function(err, reply) {
    if (err) {
      console.log(err);
    } else {
      var id = guid();
      var message = {
        "id" : id,
        "timestamp" : new Date().getTime(),
        "md5" : hash.md5(msg),
        "body" : msg
      };
      if (reply === 0) {
        that.client.sadd(workspaceKey, channelKey);
      }
      var str = JSON.stringify(message);
      that.client.set(id, str);
      that.client.lpush(channelKey, id);
    }
    if (callback) {
      callback(message);
    }
  });
};

Client.prototype._peekAt = function(wid, cid, index, callback) {
  var channelKey = this._channelKey(wid, cid);
  var that = this;
  var visibility = 30; // seconds
  this.client.lindex(channelKey, index, function(err, id) {
    if (err) {
      console.log(err);
      callback(null);
    } else {
      if (id === null) {
        callback(null);
      } else {
        // check to see if this message id is 'reserved'
        var lock = id + ':lock';
        that.client.exists(lock, function(locked) {
          if (locked) {
            that._peekAt(wid, cid, index - 1, callback);
          } else {
            // reserve the message for 'visibility' seconds, effectively
            // making it "invisible" to other clients. `lock` is used for
            // checking elements while doing `lindex`, while `handle` is given
            // to the client and used for `del` ops.
            that.client.setex(lock, visibility, null, function(err) {
              that.client.get(id, function(err, message) {
                if (err) {
                  console.log(err);
                  callback(null);
                } else {
                  if (message) {
                    var receiptHandle = guid();
                    message.receiptHandle = receiptHandle;
                    that.client.setex(that._receiptHandleKey(receiptHandle), visibility, message.id, function(err) {
                      if (err) {
                        console.log(err);
                        callback(null);
                      } else {
                        // used for del
                        callback(JSON.parse(message));                      
                      }
                    });
                  } else {
                    // FIXME: found a dangling reference in the channel,
                    // delete element by index from the list
                  }
                }
              });
            });
          }
        });
      }
    }
  });
};

Client.prototype.peek = function(wid, cid, callback) {
  this._peekAt(wid, cid, -1, callback);
};

Client.prototype.del = function(receiptHandle, callback) {
  if (receiptHandle) {
    var that = this;
    this.client.get(this._receiptHandleKey(receiptHandle), function(err, id) {
      if (err) {
        console.log(err);
        callback(null);
      } else {
        that.client.get(id, function(err, message) {
          if (err) {
            console.log(err);
            callback(null);
          } else {
            // this will delete this message, but leave a dangling reference in the queue
            that.client.del(id);
            callback(JSON.parse(message));
          }
        });
      }
    });
  } else {
    callback(null);
  }
};

Client.prototype.channels = function(wid, callback) {
  if (callback) {
    var workspaceKey = this._workspaceKey(wid);
    this.client.smembers(workspaceKey, function(err, replies) {
      if (err) {
        console.log(err);
      }
      callback(replies);
    });
  }
};

Client.prototype.length = function(wid, cid, callback) {
  if (callback) {
    var channelKey = this._channelKey(wid, cid);
    this.client.llen(channelKey, function(err, length) {
      if (err) {
        console.log(err);
      }
      callback(length);
    });
  }
};

module.exports.createClient = function() {
  return new Client();
};

module.exports.Client = Client;
