var http = require('http');

module.exports = function messageWriter(scheme, hostname, port) {
  function host(scheme, hostname, port) {
    if (port === 80 && scheme === 'http' ||
        port === 443 && scheme === 'https') {
      return hostname;
    } else {
      return hostname + ':' + port;
    }
  };

  var urlbase = scheme + '://' + host(scheme, hostname, port);

  return function messageWriter(req, res, next) {
    req.urlbase = urlbase;
    
    res.writeError = function(code, err) {
      res.writeText(err, code);
    };

    res.writeText = function(text, code) {
      code = code || 500;
      text = text || "";
      res.writeHead(code, {
        "Content-Type" : "text/plain; charset=utf-8",
        "Content-Length" : text.length
      });
      res.end(text);
    };

    res.writeJSON = function(json, code, headers) {
      code = code || 200;
      if (json) {
        var buffer = new Buffer(JSON.stringify(json));
        var defaultHeaders = {
          "Content-Type" : "application/json",
          "Content-Length" : buffer.length
        };
        headers = headers || defaultHeaders;
        if (headers != defaultHeaders) {
          for (var attrName in defaultHeaders) {
            if (!headers.hasOwnProperty(attrName)) {
              headers[attrName] = defaultHeaders[attrName];
            }
          }
        }
      }
      res.writeHead(code, headers);
      res.end(buffer);
    };

    next();
  };
};
