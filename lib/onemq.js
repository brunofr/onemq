var connect = require('connect'),
    rest = require('./rest');

var port = 3000;

var restApp = rest.createServer('http', 'api.onemq.com', port, '/');

var server = connect.createServer(connect.vhost(restApp.hostname, restApp));

server.listen(port);
